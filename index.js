require("dotenv").config();

const AWS = require("aws-sdk");
const Responses = require("./apiResponses");
const Utility = require("./utility");

const client = require("twilio")(
  process.env.TWILIO_SID,
  process.env.TWILIO_AUTH
);

// set operating timezone to UTC
process.env.TZ = "Europe/Amsterdam";

let awsConfig = {
  region: "ap-southeast-1",
  endpoint: "http://dynamodb.ap-southeast-1.amazonaws.com",
  accessKeyId: process.env.AWS_ACCESS_ID,
  secretAccessKey: process.env.AWS_ACCESS_SECRET,
};

AWS.config.update(awsConfig);

// Immediately-invoked Function Expression (IIFE) in order to use aync/await
(async () => {
  console.log(await main());
})();

async function main() {
  const documentClient = new AWS.DynamoDB.DocumentClient();

  // get all messages that are scheduled in the current hour
  let dateKey = new Date();
  dateKey.setUTCMinutes(0);
  dateKey.setUTCSeconds(0);

  const dateKeyString = dateKey.toISOString().slice(0, 19);
  let totalMessagesSent = 0;
  let totalMessagesFailedToSend = 0;

  const paramsDB = {
    TableName: process.env.MESSAGES_TABLE_NAME,
    IndexName: "MessageSendDateTime-MessageStatus-index",
    ExpressionAttributeValues: {
      ":MessageSendDateTime": dateKeyString,
      ":MessageStatus": "pending",
    },
    KeyConditionExpression:
      "MessageSendDateTime = :MessageSendDateTime and MessageStatus = :MessageStatus",
  };

  let messages = "";

  try {
    const data = await documentClient.query(paramsDB).promise();
    messages = data.Items;
  } catch (err) {
    return Responses._403(`Unable to query message data: ${err}`);
  }

  if (Object.keys(messages).length === 0) {
    return Responses._200(
      "Function successfully executed. There are no scheduled messages at this time"
    );
  }

  for (const message of messages) {
    const customerNumber = Utility.getE164Format(
      message.CustomerCountryCode,
      message.CustomerPhoneNumber
    );

    try {
      let smsInfo = await client.messages.create({
        body: message.MessageContent,
        from: process.env.TWILIO_PHONE_NUMBER,
        statusCallback: process.env.LOG_SMS_URL,
        to: customerNumber,
      });

      totalMessagesSent++;
      const params = {
        TableName: process.env.LOGS_TABLE_NAME,
        Item: {
          SmsId: smsInfo.sid,
          MessageId: message.MessageId,
          CreatedTimeStamp: new Date().toISOString().slice(0, 19),
          ClientId: message.ClientId,
          CustomerId: message.CustomerId,
          CustomerNumber: message.CustomerNumber,
          SmsContent: message.MessageContent,
          SmsStatus: "Accepted",
        },
      };

      try {
        // create logs for sent SMS
        await documentClient.put(params).promise();
      } catch (err) {
        return Responses._403(`Unable to log SMS data: ${err}`);
      }

      if (message.Recurring === "true") {
        let recurringParams = getRecurringParams(message);
        try {
          await documentClient.update(recurringParams).promise();
        } catch (err) {
          return Responses._403(`Unable to schedule recurring message: ${err}`);
        }
      } else {
        let markCompleteParams = getMarkCompleteParams(message);
        try {
          await documentClient.update(markCompleteParams).promise();
        } catch (err) {
          return Responses._403(`Unable to mark message as completed: ${err}`);
        }
      }
    } catch (err) {
      // assume error is thrown by Twilio if code field is provided
      if (!err.code) {
        return Responses._500(`Something went wrong: ${err}`);
      }

      totalMessagesFailedToSend++;
      let failedParams = getFailedParams(message, err);
      try {
        // mark message status as failed
        await documentClient.update(failedParams).promise();
      } catch (err) {
        return Responses._403(`Unable to mark message as failed: ${err}`);
      }
    }
  }

  return Responses._200(
    `Function successfully executed. Total Messages sent: ${totalMessagesSent}. Total Messages failed to send: ${totalMessagesFailedToSend}`
  );
}

function getRecurringParams(message) {
  let nextRecurringDate = Utility.getNextRecurringDate(
    message.LocalMessageSendDate,
    message.RecurringFrequency,
    message.RecurringDay
  );

  // for daily, increment to the next closest operation day if recurring day is not within operation day
  if (message.RecurringFrequency === "daily") {
    while (
      !message.OperationDays.includes(
        Utility.dayOfWeekAsString(nextRecurringDate.getUTCDay())
      )
    ) {
      nextRecurringDate.setUTCDate(nextRecurringDate.getUTCDate() + 1);
    }
  }

  // for monthly, decrement till the previous closest operation day if recurring day is not within operation day
  if (message.RecurringFrequency === "monthly") {
    while (
      !message.OperationDays.includes(
        Utility.dayOfWeekAsString(nextRecurringDate.getUTCDay())
      )
    ) {
      nextRecurringDate.setUTCDate(nextRecurringDate.getUTCDate() - 1);
    }
  }

  let endDateParts = message.LocalEndDate.split("-");
  let messageEndDate = new Date(
    Date.UTC(endDateParts[0], endDateParts[1] - 1, endDateParts[2])
  );

  // if updated send date time is after end date, mark it as completed instead
  if (nextRecurringDate.getTime() > messageEndDate.getTime()) {
    return getMarkCompleteParams(message);
  }

  const updatedLocalSendDate = nextRecurringDate.toISOString().slice(0, 10);

  nextRecurringDate.setUTCHours(
    Utility.parseTime(message.LocalMessageSendTime)
  );

  // example OperationTimeZone GMT+08:00
  let adjustmentDirection = message.OperationTimeZone.charAt(3);
  let adjustmentHour = parseInt(message.OperationTimeZone.slice(4, 6), 10);
  let adjustmentMinuite = parseInt(message.OperationTimeZone.slice(7, 9), 10);

  let adjustmentAmount;

  if (adjustmentDirection === "+" && adjustmentMinuite >= 30) {
    adjustmentAmount = adjustmentHour + 1;
  } else {
    adjustmentAmount = adjustmentHour;
  }

  // adjust recurring date to UTC time
  if (adjustmentAmount > 0) {
    nextRecurringDate.setUTCHours(
      adjustmentDirection === "+"
        ? nextRecurringDate.getUTCHours() - adjustmentAmount
        : nextRecurringDate.getUTCHours() + adjustmentAmount
    );
  }

  const recurringParams = {
    TableName: process.env.MESSAGES_TABLE_NAME,
    Key: {
      MessageId: message.MessageId,
    },
    UpdateExpression:
      "set #MessageSendDateTime = :nextRecurringDate, #LocalMessageSendDate = :updatedLocalSendDate add NumOfTimesSent :inc",
    ExpressionAttributeNames: {
      "#MessageSendDateTime": "MessageSendDateTime",
      "#LocalMessageSendDate": "LocalMessageSendDate",
    },
    ExpressionAttributeValues: {
      ":nextRecurringDate": nextRecurringDate.toISOString().slice(0, 19),
      ":updatedLocalSendDate": updatedLocalSendDate,
      ":inc": 1,
    },
  };

  return recurringParams;
}

function getMarkCompleteParams(message) {
  const completedString = "completed";

  const completeParams = {
    TableName: process.env.MESSAGES_TABLE_NAME,
    Key: {
      MessageId: message.MessageId,
    },
    UpdateExpression:
      "set #MessageStatus = :updatedStatus add NumOfTimesSent :inc",
    ExpressionAttributeNames: {
      "#MessageStatus": "MessageStatus",
    },
    ExpressionAttributeValues: {
      ":updatedStatus": completedString,
      ":inc": 1,
    },
  };

  return completeParams;
}

function getFailedParams(message, error) {
  const failedString = "failed";

  const failedParams = {
    TableName: process.env.MESSAGES_TABLE_NAME,
    Key: {
      MessageId: message.MessageId,
    },
    UpdateExpression:
      "set #MessageStatus = :updatedStatus, #TwilioErrorCode = :errorCode, #FailedTimeStamp = :currentDateTime",
    ExpressionAttributeNames: {
      "#MessageStatus": "MessageStatus",
      "#TwilioErrorCode": "TwilioErrorCode",
      "#FailedTimeStamp": "FailedTimeStamp",
    },
    ExpressionAttributeValues: {
      ":updatedStatus": failedString,
      ":errorCode": error.code,
      ":currentDateTime": new Date().toISOString().slice(0, 19),
    },
  };

  return failedParams;
}
