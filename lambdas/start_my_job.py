import json
import os
import boto3
import botocore
from botocore.exceptions import ClientError


def res_400(msg: str):
    body = {
        "message": msg,
    }
    response = {
        "statusCode": 400,
        "body": json.dumps(body)
    }
    return response


def res_200(msg: str):
    body = {
        "message": msg,
    }
    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }
    return response


def index(event, context):
    SERVICE_ID = "mass-sms"
    STRING_JOB_DEFINITION = SERVICE_ID + "-job-definition-dev"
    STRING_JOB_QUEUE = SERVICE_ID + "-job-queue-dev"

    DEFAULT_ENV = "ap-southeast-1"
    AWS_ACCESS_ID = os.environ["AWS_ACCESS_ID"]
    AWS_ACCESS_SECRET = os.environ["AWS_ACCESS_SECRET"]

    batch_client = boto3.client("batch", region_name=DEFAULT_ENV,
                                aws_access_key_id=AWS_ACCESS_ID,
                                aws_secret_access_key=AWS_ACCESS_SECRET)
                                
    try:
        batch_client.submit_job(
            jobName="mass-sms-test",
            jobQueue=STRING_JOB_QUEUE,
            jobDefinition=STRING_JOB_DEFINITION,
            containerOverrides={
                "command": ["yarn", "start"]
            }
        )
        return res_200("task initialized successfully")
    except ClientError as e:
        print(e)
        return res_400("task failed to initialize")

    # if "body" not in event or "date" not in event["body"]:
    #     return res_400("datestamp in format YYYY-MM-DD not provided")
