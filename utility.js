const PhoneNumber = require("awesome-phonenumber");

module.exports = {
  parseJson: (jsonString) => {
    try {
      let jsonObj = JSON.parse(jsonString);

      if (jsonObj && typeof jsonObj === "object") {
        return jsonObj;
      }
    } catch (err) {
      return false;
    }
  },
  dayOfWeekAsString: (dayIndex) => {
    return (
      [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
      ][dayIndex] || ""
    );
  },

  // example time input: 12am, 1pm etc.
  // 12am returns 0, 1pm returns 13
  // for invalid inputs, undefined is returned
  parseTime: (time) => {
    let hour = parseInt(time.slice(0, -2), 10);
    const modifier = time.slice(-2);

    if (
      hour < 1 ||
      hour > 12 ||
      (modifier.toLowerCase() != "am" && modifier.toLowerCase() != "pm") ||
      (hour < 10 && time.length != 3) ||
      (hour > 10 && time.length != 4)
    ) {
      return undefined;
    }

    if (hour === 12) {
      hour = 0;
    }

    if (modifier.toLowerCase() === "pm") {
      hour += 12;
    }

    return hour;
  },

  checkPhoneNumberValidity: (countryCode, phoneNumber) => {
    if (countryCode.charAt(0) != "+") {
      return false;
    }

    const regionCode = PhoneNumber.getRegionCodeForCountryCode(
      countryCode.substring(1)
    );
    const phoneNumberObject = new PhoneNumber(phoneNumber, regionCode);

    return phoneNumberObject.isValid();
  },

  getE164Format: (countryCode, phoneNumber) => {
    const regionCode = PhoneNumber.getRegionCodeForCountryCode(
      countryCode.substring(1)
    );
    const phoneNumberObject = new PhoneNumber(phoneNumber, regionCode);

    return phoneNumberObject.getNumber("e164");
  },

  getNextRecurringDate: (messageSendDate, recurringFrequency, recurringDay) => {
    let nextRecurringDate;

    if (messageSendDate instanceof Date) {
      nextRecurringDate = messageSendDate;
    } else {
      let dateParts = messageSendDate.split("-");
      nextRecurringDate = new Date(
        Date.UTC(dateParts[0], dateParts[1] - 1, dateParts[2])
      );
    }

    if (recurringFrequency === "daily") {
      nextRecurringDate.setUTCDate(nextRecurringDate.getUTCDate() + 1);
    } else if (recurringFrequency === "weekly") {
      nextRecurringDate.setUTCDate(nextRecurringDate.getUTCDate() + 1);
      nextRecurringDate.setUTCDate(
        nextRecurringDate.getUTCDate() +
          ((recurringDay + 7 - nextRecurringDate.getUTCDay()) % 7)
      );
    } else if (recurringFrequency === "monthly") {
      if (nextRecurringDate.getUTCDate() >= recurringDay) {
        // next month
        nextRecurringDate.setUTCMonth(nextRecurringDate.getUTCMonth() + 1);
        const daysInMonth = new Date(
          Date.UTC(
            nextRecurringDate.getUTCFullYear(),
            nextRecurringDate.getUTCMonth() + 1,
            0
          )
        ).getUTCDate();
        if (recurringDay > daysInMonth) {
          // if recurring day is above the number of days in the month, the send date will be set to the last day of the month
          nextRecurringDate.setUTCDate(daysInMonth);
        } else {
          nextRecurringDate.setUTCDate(recurringDay);
        }
      } else {
        // current month
        const daysInMonth = new Date(
          Date.UTC(
            nextRecurringDate.getUTCFullYear(),
            nextRecurringDate.getUTCMonth() + 1,
            0
          )
        ).getUTCDate();
        if (recurringDay > daysInMonth) {
          // if recurring day is above the number of days in the month, the send date will be set to the last day of the month
          nextRecurringDate.setUTCDate(daysInMonth);
        } else {
          nextRecurringDate.setUTCDate(recurringDay);
        }
      }
    }

    return nextRecurringDate;
  },
};
