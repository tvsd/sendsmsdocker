### Docker commands

```sh
# building the container
docker build -t test .
# running the container (works from anywhere)
docker run test
# running + need input/output (works from project root directory)
docker run -v output:/output -v input:/input test
```