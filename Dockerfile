from ubuntu:latest

# run nodesource...latest
# run yarn

run apt-get update
run apt-get install -y curl
run curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
run apt-get install -y nodejs

run curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
run echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
run apt-get update && apt-get install -y yarn

copy . .
# run npm install # replace to use npm
run yarn

# cmd ["npm", "start"] # replace to use npm
# cmd ["ls", "/output"]
cmd ["yarn", "start"]
